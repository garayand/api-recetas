const mongoose = require('mongoose')
const  ObjectID = require('mongodb').ObjectId

const ingredientSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        lowercase: true
    },
    qty: {
        type: Number,
        required: true
    },
    unit: {
        type: String,
        required: true,
        lowercase: true
    }
}, { _id: false})

const ratingSchema = mongoose.Schema({
    userId: {
        type: ObjectID,
        required: true
    },
    rating: {
        type: Number,
        required: true,
        validate: {
            validator: v => v >= 0 && v<=5,
            message: "rating debe estar entre 0 y 5"
        }
    },
}, { _id: false})

const recipeSchema = mongoose.Schema({
    schema: {
        type: Number,
        required: true
    },
    userId: {
        type: ObjectID,
        required: true
    },
    name: {
        type: String,
        required: true,
        lowercase: true,
    },
    ingredients: {
        type: [ingredientSchema],
        validate: {
            validator: v => v.length > 0,
            message: "Sin ingredientes"
        }
    },
    method: {
        type: String,
        required: true,
        lowercase: true,
    },
    ratings: [ratingSchema],
    avgRating: Number
})

module.exports = mongoose.model('recepies', recipeSchema)
