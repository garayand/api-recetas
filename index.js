const express = require('express');
const mongoose = require('mongoose');
const userSchema = require('./models/user')
const recipeSchema = require('./models/recipe')
const  ObjectID = require('mongodb').ObjectId


const app = express();

// MongoDB connection
//mongoose.connect('mongodb://agaray:recipes@localhost:27017/recipesdb?authSource=admin')
mongoose.connect('mongodb://agaray:recipes@mongo_recetas:27017/recipesdb?authSource=admin')

.then(() => console.log('conectado a Mongo'))
.catch((error) => console.error(error))

//Middleware
app.use(express.json())

// Routes
app.post('/new_user', async (req,res) => {
    const user = userSchema(req.body);
    try {
        const dataToSave = await user.save();
        res.status(200).json(dataToSave)
    }
    catch (error) {
        res.status(400).json({message: error.message})
    }
})

app.get('/users', async (req,res) => {
    try {
        const data = await userSchema.find();
        res.json(data)
    }
    catch (error) {
        res.status(400).json({message: error.message})
    }
})

app.post('/new_recipe', async (req,res) => {
    const recipe = recipeSchema(req.body);
    try {
        const dataToSave = await recipe.save();
        res.status(200).json(dataToSave)
    }
    catch (error) {
        res.status(400).json({message: error.message})
    }
})

app.post('/rate', (req,res) => {
    const { recipeId, userId , rating } = req.body
    recipeSchema.updateOne(
        { _id: recipeId },
        [
            { $set: { ratings: {$concatArrays: [{$ifNull: ['$ratings', []]}, [{ userId: userId, rating: rating}]]}} },
            {$set: {avgRating: {$trunc: [{$avg:['$ratings.rating']},0]}}}
        ]
    )
    .then((data) => res.json(data))
    .catch((error) => {
        console.log(error)
        res.send("error")
    })
})

app.get('/recipes', async (req,res) => {
    const user = req.body.userId;
    try {
        let collection = await recipeSchema.find({ "userId": new ObjectID(user) });
        res.json(collection)
    }
    catch (error) {
        res.status(404).json({message: error.message})
    }
})

app.get('/recipesbyingredient', async (req,res) => {
    const { ingredients } = req.body;
    const ingredientsArray = ingredients.map(a => a.name);

    try {
        let collection = await recipeSchema.find(
                { $expr: { $setIsSubset: [ "$ingredients.name", ingredientsArray ] } }
            );
        res.json(collection)
    }
    catch (error) {
        res.status(404).json({message: error.message})
    }
})

app.listen(3000, () => console.log("Esperando en puerto 3000..."))
